import React from "react";
import { graphql, Link } from "gatsby";
import { getAllCompaniesTags } from "../../utils/companies.js";
import CompanyTag from "../../components/company/tag.js";
import IndexList from "../../components/index/list.js";
import IndexToc from "../../components/index/toc.js";

export default function TagsCompaniesPage({
	data: {
		companies: { edges: companies },
	},
}) {
	const tags = getAllCompaniesTags(companies).sort((a, b) => a > b);

	/* build an alphabetic index of tags */
	const tagIndex = tags.reduce((index, tag) => {
		const indexLetter = tag.slice(0, 1).toLowerCase();
		index[indexLetter] = index[indexLetter] || [];
		index[indexLetter].push(tag);
		return index;
	}, {});

	return (
		<>
			<header className="page__header">
				<h1 className="page__title">
					All <Link to={"/companies"}>companies</Link> category{" "}
					<Link to={"/tags"}>tags</Link>
				</h1>
				<p className="page__description">
					There are {tags.length} tags used across{" "}
					<Link to={"/companies"}>all companies</Link> to group the projects in
					topics.
				</p>
			</header>
			{tags ? (
				<main className="page__main page__main--index">
					<menu className="nav nav--index">
						<IndexToc index={tagIndex}></IndexToc>
					</menu>
					<main className="page__content">
						<IndexList index={tagIndex}>
							{(indexTerms) => (
								<div className="tags">
									<ul>
										{indexTerms.map((tag) => (
											<li key={tag} className="nav__item">
												<CompanyTag tag={tag} />
											</li>
										))}
									</ul>
								</div>
							)}
						</IndexList>
					</main>
				</main>
			) : (
				<p>There are no tags.</p>
			)}
		</>
	);
}

export const query = graphql`
	{
		companies: allMarkdownRemark(
			filter: { fileAbsolutePath: { regex: "//(companies)/" } }
			sort: { order: DESC, fields: frontmatter___created_at }
		) {
			edges {
				node {
					...CompanyCardFragment
				}
			}
		}
	}
`;
