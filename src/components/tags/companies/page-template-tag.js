import React from "react";
import { graphql, Link } from "gatsby";

import CompanyList from "../../company/list";

export default function TagTemplate({
	pageContext: { slug },
	data: {
		companies: { edges: companies },
	},
}) {
	return (
		<>
			<header className="page__header">
				<h1 className="page__title">
					<Link to={`/tags/companies/${slug}`} className="tag">
						{slug}
					</Link>
				</h1>
				<p className="page__description">
					Listing <strong>{companies.length}</strong>{" "}
					<Link to={"/companies"}>
						compan{companies.length > 1 ? "ies" : "y"}
					</Link>{" "}
					described with{" "}
					<strong>
						<Link to={`/tags/companies/${slug}`}>{slug}</Link>
					</strong>{" "}
					as <Link to={"/tags/companies"}>tag</Link>.
				</p>
			</header>
			<main className="page__main">
				{companies && <CompanyList list={companies} />}
			</main>
		</>
	);
}

export const query = graphql`
	query ($slug: String!) {
		companies: allMarkdownRemark(
			filter: {
				fileAbsolutePath: { regex: "//(companies)/" }
				frontmatter: { tags: { in: [$slug] } }
			}
			sort: { order: DESC, fields: frontmatter___created_at }
		) {
			edges {
				node {
					...CompanyCardFragment
				}
			}
		}
	}
`;
