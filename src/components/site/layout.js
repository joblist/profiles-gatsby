import React from "react";
import SiteMenu from "./menu";

export default function SiteLayout({ children }) {
	return (
		<>
			<SiteMenu />
			<joblist-page>{children}</joblist-page>
		</>
	);
}
