import React from "react"
import CompanyCard from './card'

export default function CompanyList({
	list
}) {
	return (
		<div className="companies-list">
			{!list ? (
				<div>No items.</div>
			) : (
				<ul>
					{list.map(({node}) => (
						<li key={node.id}>
							<CompanyCard company={node}/>
						</li>
					))}
				</ul>
			)}
		</div>
	)
}
