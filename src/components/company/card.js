import React from "react";
import { graphql, Link } from "gatsby";

import Tag from "./tag";

function CompanyLinks({ navLinks, socialLinks }) {
	return (
		<>
			{navLinks && (
				<nav className="company-card__links-nav nav nav--horizontal">
					<ul>
						{navLinks.map((link, index) => (
							<li key={index}>
								<a href={link.url} className="nav__item">
									{link.name}
								</a>
							</li>
						))}
					</ul>
				</nav>
			)}
			{socialLinks && (
				<nav className="company-card__links-nav nav nav--horizontal">
					<ul>
						{socialLinks.map((link, index) => {
							const url = new URL(link.url);
							url.searchParams.set("utm_source", "joblist.today");
							url.searchParams.set("utm_campaign", "profiles.joblist.today");
							return (
								<li key={index}>
									<a href={url} className="nav__item">
										{link.name}
									</a>
								</li>
							);
						})}
					</ul>
				</nav>
			)}
		</>
	);
}

export default function CompanyCard({
	full,
	company: {
		fields: { slug },
		frontmatter: {
			title,
			tags,
			description,
			job_board_url,
			company_url,
			linkedin_url,
			twitter_url,
			wikipedia_url,
			facebook_url,
			instagram_url,
			created_at,
			updated_at,
		},
	},
}) {
	const navLinks = [
		{
			name: company_url,
			url: company_url,
		},
		{
			name: job_board_url,
			url: job_board_url,
		},
		{
			name: "wikipedia",
			url: wikipedia_url,
		},
	].filter(({ url }) => url);

	const socialLinks = [
		{
			name: "twitter",
			url: twitter_url,
		},
		{
			name: "linkedin",
			url: linkedin_url,
		},
		{
			name: "facebook",
			href: facebook_url,
		},
		{
			name: "instagram",
			url: instagram_url,
		},
	].filter(({ url }) => url);

	return (
		<article className="company-card">
			<header className="company-card__header">
				<h1 className="company-card__title">
					<Link to={`/companies/${slug}`}>{title}</Link>
				</h1>
			</header>
			{tags && (
				<footer className="company-card__footer">
					<nav className="company-card__tags tags">
						<ul>
							{tags.map((tag) => (
								<li key={tag}>
									<Tag tag={tag} />
								</li>
							))}
						</ul>
					</nav>
				</footer>
			)}
			<main className="company-card__main">
				{description && (
					<div className="company-card__description">{description}</div>
				)}
				{full ? (
					<CompanyLinks navLinks={navLinks} socialLinks={socialLinks} />
				) : null}
			</main>
		</article>
	);
}

export const query = graphql`
	fragment CompanyCardFragment on MarkdownRemark {
		id
		fields {
			slug
		}
		frontmatter {
			created_at(formatString: "YYYY-MM-DD")
			updated_at(formatString: "YYYY-MM-DD")
			title
			tags
			description
			is_featured
			job_board_hostname
			job_board_provider
			job_board_url
			company_url
			linkedin_url
			twitter_url
			wikipedia_url
			facebook_url
			instagram_url
			positions {
				map
			}
		}
	}
`;
