const path = require("path");

/* source the correctly named `.env{.NODE_ENV}` file */
require("dotenv").config({
	path: `.env.${process.env.NODE_ENV}`,
});

if (process.env.VERBOSE) {
	const {
		VERBOSE,
		NODE_ENV,
		CI_COMMIT_REF_NAME,
		CI_PAGES_DOMAIN,
		PREFIX_PATH = "/",
	} = process.env;
	console.info({
		message: "Gatsby project process.env",
		env: {
			VERBOSE,
			NODE_ENV,
			CI_COMMIT_REF_NAME,
			CI_PAGES_DOMAIN,
			PREFIX_PATH,
		},
	});
}

module.exports = {
	/* when hosted on gitlab `npm run build --prefix-paths` */
	pathPrefix: process.env.PREFIX_PATH,

	siteMetadata: {
		title: "JobList Profiles",
		siteUrl: "https://profiles.joblist.today",
	},

	plugins: [
		/* clone the database, a git repo with markdown files:
			 - use: https://www.npmjs.com/package/gatsby-plugin-git-clone
			 - not (depends on "git"): https://www.gatsbyjs.com/plugins/gatsby-source-git */
		{
			resolve: "gatsby-plugin-git-clone",
			options: {
				repository: "https://github.com/joblisttoday/data.git",
				branch: `main`,
				path: "./content",
			},
		},

		/* source our different content types */
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `companies`,
				path: path.resolve(`content/companies`),
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `tags`,
				path: path.resolve(`content/tags`),
			},
		},

		/* transform the sourced content to markdown */
		`gatsby-transformer-remark`,
	],
};
